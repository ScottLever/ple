﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgrammingLearningEnvironment
{
    public partial class Form1 : Form
    {
        //initialization
        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvass MyCanvass;

        public Form1()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(OutputBitmap));
            MyCanvass.Clear(); //Refreshes the canvass to start with correct values and color
            Refresh();
        }

        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter) //only reacts to enter input on commandline
                {
                    String Command = commandLine.Text.Trim(); //removes excess spaces from the ends ONLY
                    string[] split = Command.Split(' '); // splits by space characters for invidual components
                    if (split[0].Equals("drawTo") == true) //enters to drawTo command
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);   
                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640)) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawLine(num1, num2); //draws a line from the held xPos/yPos in canvass class to two coordinates(num1, num2)
                            Console.WriteLine("LINE"); //displays in console for clarification
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //enters this else if the numbers entered are too big for the canvass
                        }

                    }
                    else if (split[0].Equals("square") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawSquare(num1); //draws a square around the held xPos/yPos in canvass class by side length: num1
                            Console.WriteLine("SQUARE");//displays in console for clarification   
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0);//enters this else if the numbers entered are too big for the canvass
                        }
                    }
                    else if (split[0].Equals("rectangle") == true)  
                    {
                        int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                        int num2 = Convert.ToInt32(split[2]);   
                        ;
                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawRectangle(num1, num2); //draws rectangle width, height around xPos yPos
                            Console.WriteLine("RECTANGLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0);//number out of canvass range
                        }
                    }
                    else if (split[0].Equals("circle") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]); //converts initial string value of number to an int

                        if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                        {
                            MyCanvass.DrawCircle(num1); //draws circle with radius - num1-  around xPos yPos
                            Console.WriteLine("CIRCLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //number out of canvass range
                        }
                    }
                    else if (split[0].Equals("moveTo") == true) //moves pen without drawing
                    {
                        int num1 = Convert.ToInt32(split[1]);
                        int num2 = Convert.ToInt32(split[2]);
                       
                        if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640)) //numbers in range of canvass
                        {
                            MyCanvass.moveTo(num1, num2); //moves pen to coords
                            Console.WriteLine("Pen Moved");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); // error for out of range 
                        }
                    }
                    else if (split[0].Equals("clear") == true) //same as clear button resets to blank canvass
                    {
                        MyCanvass.Clear();
                    }
                    else if (split[0].Equals("triangle") == true)
                    {
                        int num1 = Convert.ToInt32(split[1]);
                        
                        if (num1 > 0 && num1 < 640) //canvass range
                        {
                            MyCanvass.DrawTriangle(num1); //draws triangle of size - num1 - around xpos yPos
                            Console.WriteLine("TRIANGLE");
                        }
                        else
                        {
                            MyCanvass.PrintError(6, 0); //canvass range error
                        }
                    }
                    else if (split[0].Equals("reset") == true) //sets pen coords to 0,0. doesnt clear canvass
                    {
                        MyCanvass.moveTo(0, 0);
                        Console.WriteLine("Pen Moved");
                    }
                    else if (split[0].Equals("pen") == true) //changes pen color to 1 0f 4 colors
                    {
                        String color = split[1];
                        if (color == "red" || color == "green" || color == "yellow" || color == "black")
                        {
                            MyCanvass.penColor(color);
                            Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                        }
                        else
                        {
                            MyCanvass.PrintError(3, 0); //error if they type color that is not an option
                        }
                    }
                    else if (split.Length >= 4) //checks amount of inputs
                    {
                        MyCanvass.PrintError(0, 0);//Too many parameters 
                    }
                    else if (split[0].Equals("fill") == true) //allows for the user to have the fill option on or off (y/n)
                    {
                        if (split[1].Equals("y") == true)
                        {
                            MyCanvass.fillYN(true);
                        }
                        else if (split[1].Equals("n") == true)
                        {
                            MyCanvass.fillYN(false);
                        }
                        else
                        {
                            MyCanvass.PrintError(4, 0); //error for not a valid fill option
                        }
                    }
                    else if (split[0].Equals("run") == true) //allows run to be typed in command line or pressed as a button
                    {
                        run();
                    }
                    else
                    {
                        MyCanvass.PrintError(2, 0);//incorrect commnad
                    }
                    commandLine.Text = "";
                    Refresh();
                }
            }catch(System.FormatException)// catches number format exception e.g float or letters
            {
                MyCanvass.PrintError(5, 0);
                Refresh();
            }
            catch(System.IndexOutOfRangeException)// catches number format exception e.g float or letters
            {
                MyCanvass.PrintError(7, 0);
                Refresh();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)//graphics
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        private void run_Click(object sender, EventArgs e)
        {
            run();
        }
        public void parseCommand(String Line, int lNum)//run() passes commands here with their line number
        {
            try
            {
                String Command = Line.Trim(); //removes excess spaces from the ends ONLY
                string[] split = Command.Split(' ');    // splits by space characters for invidual components
                if (split[0].Equals("drawTo") == true)  //enters to drawTo command
                {
                    int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                    int num2 = Convert.ToInt32(split[2]);
                    
                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawLine(num1, num2); //draws a line from the held xPos/yPos in canvass class to two coordinates(num1, num2)
                        Console.WriteLine("LINE");  //displays in console for clarification
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, lNum); not displayed because of syntax button
                    }
                }
                else if (split[0].Equals("square") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int
                    if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawSquare(num1); //draws a square around the held xPos/yPos in canvass class by side length: num1
                        Console.WriteLine("SQUARE");    //displays in console for clarification 
                    }
                    else
                    {
                       // MyCanvass.PrintError(6, 0);
                    }

                }

                else if (split[0].Equals("rectangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);    //converts initial string value of number to an int
                    int num2 = Convert.ToInt32(split[2]);
                    
                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawRectangle(num1, num2);    //draws rectangle width, height around xPos yPos
                        Console.WriteLine("RECTANGLE");
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, lNum);
                    }
                }
                else if (split[0].Equals("circle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);   //converts initial string value of number to an int

                    if (num1 > 0 && num1 < 640) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                    {
                        MyCanvass.DrawCircle(num1);  //draws circle with radius - num1-  around xPos yPos
                        Console.WriteLine("CIRCLE");
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, 0);
                    }
                }
                else if (split[0].Equals("triangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                   
                    if (num1 > 0 && num1 < 640) //canvass range
                    {
                        MyCanvass.DrawTriangle(num1);    //draws triangle of size - num1 - around xpos yPos
                        Console.WriteLine("TRIANGLE");
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, 0);
                    }
                }
                else if (split[0].Equals("moveTo") == true) //moves pen without drawing
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);
                   
                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))   //numbers in range of canvass
                    {
                        MyCanvass.moveTo(num1, num2);   //moves pen to coords
                        Console.WriteLine("Pen Moved");
                    }
                    else
                    {
                        //MyCanvass.PrintError(6, lNum);
                    }
                }
                else if (split[0].Equals("clear") == true)  //same as clear button resets to blank canvass
                {
                    MyCanvass.Clear();
                }
                else if (split[0].Equals("reset") == true)  //sets pen coords to 0,0. doesnt clear canvass
                {
                    MyCanvass.moveTo(0, 0);
                    Console.WriteLine("Pen Moved");
                }
                else if (split[0].Equals("pen") == true)    //changes pen color to 1 0f 4 colors
                {
                    String color = split[1];
                    if (color == "red" || color == "green" || color == "yellow" || color == "black")
                    {
                        MyCanvass.penColor(color);
                        Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                    }
                    else
                    {
                        //MyCanvass.PrintError(3, lNum);
                    }
                }
                else if (split.Length >= 4) //checks amount of inputs
                {
                    //MyCanvass.PrintError(0, lNum);//Too many parameters 
                }
                else if (split[0].Equals("fill") == true)   //allows for the user to have the fill option on or off (y/n)
                {
                    if (split[1].Equals("y") == true)
                    {
                        MyCanvass.fillYN(true);
                    }
                    else if (split[1].Equals("n") == true)
                    {
                        MyCanvass.fillYN(false);
                    }
                    else
                    {
                        //MyCanvass.PrintError(4, 0);
                    }
                }
                else
                {
                   // MyCanvass.PrintError(2, lNum);//incorrect commnad
                }
                commandLine.Text = "";
                Refresh();
            }
            catch (System.FormatException)  // catches number format exception e.g float or letters
            {
               // MyCanvass.PrintError(5, 0);
                Refresh();
            }
            
            
        }

        private void Clear_Click(object sender, EventArgs e)//clears canvass and resets xPos yPos
        {
            MyCanvass.Clear();
            Refresh();

        }

        private void Syntax_Click(object sender, EventArgs e)//sends lines to CheckingSyntax to be checked for errors
        {
            if (richTextBox1.Text != "")
            {
                for (int i = 0; i < richTextBox1.Lines.Length; i++)//loop to check each line
                {
                    String Line = richTextBox1.Lines[i];
                    CheckingSyntax(Line, (i + 1)); //i+1 is the line number being passed
                }
            }
            else
            {
                MyCanvass.PrintError(1, 0);//text area is empty
                Refresh();
            }
        }
        private void CheckingSyntax(String Line, int lNum)//runs the same code as parseCommand without running the commnands, instead checking for errors
        {
            try
            {
                String Command = Line.Trim();
                string[] split = Command.Split(' ');
                if (split[0].Equals("drawTo") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))
                    {
                        
                        Console.WriteLine("LINE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("square") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    if (num1 > 0 && num1 < 640)
                    {
                       
                        Console.WriteLine("SQUARE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }

                }

                else if (split[0].Equals("rectangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))
                    {
                        
                        Console.WriteLine("RECTANGLE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("circle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);

                    if (num1 > 0 && num1 < 640)
                    {
                       
                        Console.WriteLine("CIRCLE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("triangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);

                    if (num1 > 0 && num1 < 640)
                    {
                        
                        Console.WriteLine("TRIANGLE");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("moveTo") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);

                    if ((num1 > 0 && num1 < 640) && (num2 > 0 && num2 < 640))
                    {
                        
                        Console.WriteLine("Pen Moved");
                    }
                    else
                    {
                        MyCanvass.PrintError(6, lNum);//too big for canvass
                    }
                }
                else if (split[0].Equals("clear") == true)
                {
                    
                }
                else if (split[0].Equals("reset") == true)
                {
                   
                    Console.WriteLine("Pen Moved");
                }
                else if (split[0].Equals("pen") == true)
                {
                    String color = split[1];
                    if (color == "red" || color == "green" || color == "yellow" || color == "black")
                    {
                        Console.WriteLine("PEN COLOR CHANGED TO: ", color);
                    }
                    else
                    {
                        MyCanvass.PrintError(3, lNum); //incorrect pen color
                    }
                }
                else if (split.Length >= 4)
                {
                    MyCanvass.PrintError(0, lNum);//Too many parameters 
                }
                else if (split[0].Equals("fill") == true)
                {
                    if (split[1].Equals("y") == true)
                    {
                        
                    }
                    else if (split[1].Equals("n") == true)
                    {
                        
                    }
                    else
                    {
                        MyCanvass.PrintError(4, lNum);//invalid fill option
                    }
                }
                else
                {
                    MyCanvass.PrintError(2, lNum);//incorrect commnad
                }
                commandLine.Text = "";
                Refresh();
            }
            catch (System.FormatException)
            {
                MyCanvass.PrintError(5, lNum);//number invalid
                Refresh();
            }
        }

        private void reset_Click(object sender, EventArgs e)//resets pen to (0,0) wihtout clearing canvass
        {
            MyCanvass.moveTo(0, 0);
            Refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    String fileName = openFileDialog1.FileName;
                    String fileText = File.ReadAllText(fileName);
                    richTextBox1.Text = fileText;
                    myStream.Close();
                }
            }
            

        }//opens file to load into richtextBox

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using(var sfd = new SaveFileDialog())
            {
                sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                sfd.FilterIndex = 2;
                if(sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, richTextBox1.Text);
                }
            }
            MyCanvass.Clear();
            Refresh();
            richTextBox1.Text = "";
        }//saves file from richtextBox to .txt file

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyCanvass.About();
            Refresh();
        }//about the programme

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }//exit button
        private void run()//passes commands to parseCommand 
        {
            if (richTextBox1.Text != "")
            {
                for (int i = 0; i < richTextBox1.Lines.Length; i++)
                {
                    String Line = richTextBox1.Lines[i];
                    parseCommand(Line, i + 1);
                }

            }
            else
            {
                MyCanvass.PrintError(1, 0);//text area is empty
                Refresh();
            }
        }
    }
}
