﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml.Serialization;

namespace ProgrammingLearningEnvironment
{
    public class Canvass
    {
        Graphics g;
        Pen Pen;
        int xPos, yPos;
        int i = -20; //used for placement of error messages, to makes sure they dont overlap
        Boolean fill = false; //shape fill y/n
        Boolean dubLn = false; //for error message, double line or single line

        public Canvass(Graphics g)
        {
            //Initializing Values
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
        }

        public void DrawLine(int toX, int toY)//draws line with pen p - from xpos ypos to tox toy - sets xpos ypos equal to new values
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY); 
            xPos = toX; 
            yPos = toY;
        }
        public void DrawSquare(int width)  //draws/fills sqaure with int width
        {
            if (fill == false)
            {
                g.DrawRectangle(Pen, xPos - (width / 2), yPos - (width / 2), width, width);
            }else if (fill == true)
            {
                g.FillRectangle(Brushes.Green, xPos - (width / 2), yPos - (width / 2), width, width);
            }
        }
        public void DrawRectangle(int width, int height)    //draws/fills recatngle with int width and int height
        {
            if (fill == false)
            {
                g.DrawRectangle(Pen, xPos - (width / 2), yPos - (height / 2), width, height);
            }
            else if (fill == true)
            {
                g.FillRectangle(Brushes.Green, xPos - (width / 2), yPos - (height / 2), width, height);
            }
            
        }
        public void Clear() //clears canvass and resets xpos, ypos and i
        {
            xPos = yPos = 0;
            i = -20;
            g.Clear(Color.LightBlue);
                       
        }
        public void DrawCircle(int radius)  //draws/fills circle with int radius
        {
            Rectangle rect = new Rectangle(xPos - radius, yPos - radius, (radius * 2), (radius * 2));
            if (fill == false)
            {
                g.DrawEllipse(Pen, rect);
            }else if (fill == true)
            {
                g.FillEllipse(Brushes.Green, rect);
            }
        }
        public void DrawTriangle(int size)  //draws/fills traingle with int size
        {
            Point p1 = new Point(xPos, (yPos - size));      //coordinates for vertices of triangle 
            Point p2 = new Point(xPos + (size / 2), yPos + (size / 2));
            Point p3 = new Point(xPos - (size / 2), yPos + (size / 2));
            Point[] tPoints = //points into array
            {
                p1,
                p2,
                p3
            };
            if (fill == false)
            {
                g.DrawPolygon(Pen, tPoints);
            }
            else if (fill == true)
            {
                g.FillPolygon(Brushes.Green, tPoints);
            }
            
        }
        public void moveTo(int x, int y) //relocates xpos ypos
        {
            xPos = x;
            yPos = y;
        }
        public void penColor(String color) //sets pen color
        {
            if(color == "red")
            {
                Pen = new Pen(Color.Red, 1);
            }
            if (color == "green")
            {
                Pen = new Pen(Color.Green, 1);
            }
            if (color == "yellow")
            {
                Pen = new Pen(Color.Yellow, 1);
            }
            if (color == "black")
            {
                Pen = new Pen(Color.Black, 1);
            }
        }
        public void fillYN(Boolean f) //determines if shape will be filled or not
        {
            if (f == true)
            {
                fill = true;
            }
            if (f == false)
            {
                fill = false;
            }
        }
        public void PrintError(int num, int lineNumber) //prints error messages in pictureBox
        {
           
            using (Font myFont = new Font("Arial", 14))
            {

                if (num == 0)
                {
                    dubLn = false; //if error takes up two lines or not
                    g.DrawString("Error: Too many parameters! On Line: "+lineNumber, myFont, Brushes.Green, new Point(2, (i+20)));    //point is x+y co-ords                
                }
                if (num == 1)
                {
                    dubLn = false;
                    g.DrawString("Error: Text area is empty!", myFont, Brushes.Green, new Point(2, (i+20))); //point is x+y co-ords                
                }
                if (num == 2)
                {
                    dubLn = false;
                    g.DrawString("Error: Incorrect Command! On Line: "+lineNumber, myFont, Brushes.Green, new Point(2, (i+20))); //point is x+y co-ords                
                }
                if (num == 3)
                {
                    dubLn = false;
                    g.DrawString("Error: Not a valid pen color! On Line: "+lineNumber, myFont, Brushes.Green, new Point(2, (i+20))); //point is x+y co-ords                
                }
                if (num == 4)
                {
                    dubLn = true;
                    g.DrawString("Error: Not a valid option! On Line: " + lineNumber + "\nPlease use 'y' or 'n'", myFont, Brushes.Green, new Point(2, (i+20))); //point is x+y co-ords                
                }
                if (num == 5)
                {
                    dubLn = true;
                    g.DrawString("Error: Parameter Needs to be an Integer!\nOn Line: " + lineNumber, myFont, Brushes.Green, new Point(2, (i+20))); //point is x+y co-ords                
                }
                if (num == 6)
                {
                    dubLn = true;
                    g.DrawString("Error: Parameter needs to be between 0-640!\nOn Line: " + lineNumber, myFont, Brushes.Green, new Point(2, (i + 20))); //point is x+y co-ords                
                }
                if (num == 7)
                {
                    dubLn = false;
                    g.DrawString("Error: Invalid Parameters On Line: " + lineNumber, myFont, Brushes.Green, new Point(2, (i + 20))); //point is x+y co-ords                
                }

            }
            if (dubLn == true)
            {
                i = i + 40;
            }
            else
            {
                i = i + 20;
            }
        }
        public void About()
        {
            using (Font myFont = new Font("Arial", 14))
            {
                Clear();
                g.DrawString("Assigment Project\nDrawing Programme for Beginners\nCreated By Scott Lever", myFont, Brushes.Green, new Point(2, (i + 15)));
                xPos = 200;
                yPos = 200;
                penColor("red");
                DrawTriangle(100);
            }
        }
    }
}
