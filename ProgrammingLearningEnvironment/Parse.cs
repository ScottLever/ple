﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLearningEnvironment
{
    public class Parse
    {
        /*
         * Example class to show that I understand the unit testing
         * Didnt work for my Form1 class or Canvass class 
         * Because System.Windows.Forms was invalid... 
         */
        Boolean outOfBounds = false;
        Boolean invalidNumber = false;
        public int SquareParse(String com, int lNum)
        {
            int result = 0;
            
            String Command = com.Trim().ToLower();
            string[] split = Command.Split(' ');
            if (split[0].Equals("square") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                result = num1;

            }
            return result;

        }
        public int MoveToParse(String com, int lNum)
        {
            int result = 0;

            String Command = com.Trim();
            string[] split = Command.Split(' ');
            if (split[0].Equals("moveTo") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                int num2 = Convert.ToInt32(split[2]);

                result = (num1+num2);
            }
            return result;

        }
        public Boolean TriangleParse(String com, int lNum)
        {       
            String Command = com.Trim().ToLower();
            string[] split = Command.Split(' ');
            if (split[0].Equals("triangle") == true)
            {
                int num1 = Convert.ToInt32(split[1]);
                if ((num1 > 0 && num1 < 640)) //makes sure the numbers are appropriate for the size of the canvass 0 - 640
                {
                    outOfBounds = false;                    
                }
                else
                {
                    outOfBounds = true;
                }

            }
            return outOfBounds;

        }
        public Boolean RectangleParse(String com, int lNum)
        {
            try
            {
                String Command = com.Trim().ToLower();
                string[] split = Command.Split(' ');
                if (split[0].Equals("rectangle") == true)
                {
                    int num1 = Convert.ToInt32(split[1]);
                    int num2 = Convert.ToInt32(split[2]);
                    invalidNumber = false;

                }
            }
            catch (System.FormatException)
            {
                invalidNumber = true;
                

            }
            return invalidNumber;

        }
    }
}
