using ProgrammingLearningEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.DataAnnotations;

namespace PLETestProject
{
    [TestClass]
    public class PLETests 
    {

        [TestMethod]
        public void Square_Parameter_Is_Correct()
        {
            Parse p = new Parse();
            int actual = p.SquareParse("square 100", 0);    
            int expected = 100;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void MoveTo_Parameters_Correct()
        {
            Parse p = new Parse();
            int actual = p.MoveToParse("moveTo 160 200", 0);
            int expected = 360;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Triangle_OutOfBounds_Parameters()
        {
            Parse p = new Parse();
            var actual = p.TriangleParse("triangle 700", 0);
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Rectangle_Invalid_Number_Exception()
        {
            Parse p = new Parse();
            var actual = p.RectangleParse("rectangle 100 f", 0);
            var expected = true;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void testVar()
        {
            Var v = new Var();
            int actual = v.Value(20);
            int expected = 20;
            Assert.AreEqual(expected, actual);
        }
    }
}

